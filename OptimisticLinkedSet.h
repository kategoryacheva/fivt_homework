#pragma once

#include "arena_allocator.h"

#include <atomic>
#include <limits>
#include <mutex>

using namespace std;

///////////////////////////////////////

template <typename T>
struct ElementTraits
{
    static T Min()
    {
        return numeric_limits<T>::min();
    }
    static T Max()
    {
        return numeric_limits<T>::max();
    }
};

///////////////////////////////////////

class Spinlock
{
    atomic_flag locked{false};
public:
    explicit Spinlock() {};

    void Lock()
    {
        while (locked.test_and_set()) {};
    }

    void Unlock()
    {
        locked.clear();
    }

    // adapters for BasicLockable concept

    void lock()
    {
        Lock();
    }

    void unlock()
    {
        Unlock();
    }
};

///////////////////////////////////////

template <typename T>
class OptimisticLinkedSet
{
    struct Node
    {
        T element;
        atomic<Node*> next;
        Spinlock lock{};
        atomic<bool> marked{false};

        Node(const T& element_, Node* next_ = nullptr): element(element_), next(next_) {};
    };

    struct Edge
    {
        Node* pred;
        Node* curr;

        Edge(Node* pred_, Node* curr_): pred(pred_), curr(curr_) {};
    };


    ArenaAllocator& allocator;
    Node* head{nullptr};
    atomic<size_t> size{0};

    void CreateEmptyList()
    {
        head = allocator.New<Node>(ElementTraits<T>::Min());
        head->next = allocator.New<Node>(ElementTraits<T>::Max());
    }

    Edge Locate(const T& element) const
    {
        Node* pred = head;
        Node* curr = head->next.load();
        while (curr->element < element)
        {
            pred = curr;
            curr = curr->next.load();
        }
        return Edge{pred, curr};
    }

    bool Validate(const Edge& edge) const
    {
        return !edge.pred->marked.load() && !edge.curr->marked.load() && edge.pred->next.load() == edge.curr;
    }

public:
    explicit OptimisticLinkedSet(ArenaAllocator& allocator_): allocator(allocator_)
    {
        CreateEmptyList();
    }

    bool Insert(const T& element)
    {
        while (true)
        {
            Edge new_edge = Locate(element);
            unique_lock<Spinlock> lock_pred(new_edge.pred->lock);
            unique_lock<Spinlock> lock_curr(new_edge.curr->lock);
            if (Validate(new_edge))
            {
                if (new_edge.curr->element == element)
                {
                    return false;
                }
                Node* new_node = allocator.New<Node>(element);
                new_node->next.store(new_edge.curr);
                new_edge.pred->next.store(new_node);
                size++;
                return true;
            }
        }
    }

    bool Remove(const T& element)
    {
        while (true)
        {
            Edge node_edge = Locate(element);
            unique_lock<Spinlock> lock_pred(node_edge.pred->lock);
            unique_lock<Spinlock> lock_curr(node_edge.curr->lock);
            if (Validate(node_edge))
            {
                if (node_edge.curr->element != element)
                {
                    return false;
                }
                node_edge.curr->marked.store(true);
                node_edge.pred->next.store(node_edge.curr->next.load());
                size--;
                return true;
            }
        }
    }

    bool Contains(const T& element)
    {
        Edge node_edge = Locate(element);
        return node_edge.curr->element == element && !node_edge.curr->marked.load();
    }

    size_t Size()
    {
        return size.load();
    }
};

template <typename T> using ConcurrentSet = OptimisticLinkedSet<T>;
