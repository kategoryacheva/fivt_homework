#pragma once

#include <mutex>
#include <vector>
#include <forward_list>
#include <atomic>
#include <algorithm>

using namespace std;

template <typename T, class Hash = hash<T> >
class StripedHashSet
{
    const size_t concurrency_level;
    const size_t growth_factor;
    const double max_load_factor;
    atomic<size_t> _size;
    vector<mutex> lock_stripes;
    vector<forward_list<T> > hash_table;
    Hash hash_function;

    size_t GetBucketIndex(const size_t hash_value)
    {
        return hash_value % hash_table.size();
    }

    size_t GetStripeIndex(const size_t hash_value)
    {
        return hash_value % concurrency_level;
    }

    bool Overload()
    {
        return _size.load() / hash_table.size() > max_load_factor;
    }

    void Resize()
    {
        vector<unique_lock <mutex> > locks;
		locks.emplace_back(lock_stripes[0]);
		if (!Overload())
			return;
		for (size_t i = 1; i < lock_stripes.size(); ++i)
			locks.emplace_back(lock_stripes[i]);
		vector<forward_list<T> > tmp(growth_factor * hash_table.size());
		for (size_t i = 0; i < hash_table.size(); ++i)
			for (auto& element: hash_table[i])
			{
				auto hash_value = hash_function(element);
				tmp[hash_value % tmp.size()].push_front(element);
			}
		swap(hash_table, tmp);
    }

    bool ElementExists(const T& element, const size_t hash_value)
    {
        auto bucket = GetBucketIndex(hash_value);
        if (find(hash_table[bucket].begin(), hash_table[bucket].end(), element) != hash_table[GetBucketIndex(hash_value)].end())
            return true;
        return false;
    }

public:
    StripedHashSet(const size_t concurrency_level_,
                   const size_t growth_factor_ = 3,
                   const double max_load_factor_ = 0.75):
                       concurrency_level(concurrency_level_),
                       growth_factor(growth_factor_),
                       max_load_factor(max_load_factor_),
                       _size(0),
                       lock_stripes(concurrency_level_),
                       hash_table(concurrency_level_ * 10)
    {};

    bool Insert(const T& element)
    {
        auto hash_value = hash_function(element);
        unique_lock<mutex> lock(lock_stripes[GetStripeIndex(hash_value)]);
        if (ElementExists(element, hash_value))
            return false;
        _size.fetch_add(1);
        hash_table[GetBucketIndex(hash_value)].push_front(element);
        if (Overload())
        {
            lock.unlock();
            Resize();
        }
        return true;
    }

    bool Remove(const T& element)
    {
        auto hash_value = hash_function(element);
        unique_lock<mutex> lock(lock_stripes[GetStripeIndex(hash_value)]);
        if (!ElementExists(element, hash_value))
            return false;
        hash_table[GetBucketIndex(hash_value)].remove(element);
        _size.fetch_sub(1);
        return true;
    }

    bool Contains(const T& element)
    {
        auto hash_value = hash_function(element);
        unique_lock<mutex> lock(lock_stripes[GetStripeIndex(hash_value)]);
        return ElementExists(element, hash_value);
    }

    size_t Size()
    {
        return _size.load();
    }
};

template <typename T> using ConcurrentSet = StripedHashSet<T>;
