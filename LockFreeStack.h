#pragma once

#include <atomic>
#include <thread>

using namespace std;

template <typename T>
class LockFreeStack
{
    struct Node
    {
        T element;
        atomic<Node*> next{nullptr};
        Node(T item): element(item) {};
    };

    void PushToRubbish(Node* deleted_node)
    {
        Node* curr_top = deleted_nodes_top.load();
        deleted_node->next.store(curr_top);
        while (!deleted_nodes_top.compare_exchange_strong(curr_top, deleted_node))
            deleted_node->next.store(curr_top);
    }

    void DeleteNodes()
    {
        Node* curr_top = deleted_nodes_top.load();
        while (curr_top)
        {
            Node* node = curr_top;
            curr_top = curr_top->next;
            delete node;
        }
    }

    void DeleteStack()
    {
        Node* curr_top = top.load();
        while (curr_top)
        {
            Node* node = curr_top;
            curr_top = curr_top->next;
            delete node;
        }
    }

    atomic<Node*> top{nullptr};
    atomic<Node*> deleted_nodes_top{nullptr};

public:
    explicit LockFreeStack() {}

    ~LockFreeStack()
    {
        DeleteNodes();
        DeleteStack();
    }

    void Push(T item)
    {
        Node* new_top = new Node(item);
        Node* curr_top = top.load();
        new_top->next.store(curr_top);
        while (!top.compare_exchange_strong(curr_top, new_top))
            new_top->next.store(curr_top);
    }

    bool Pop(T& item)
    {
        while (true)
        {
            Node* curr_top = top.load();
            if (!curr_top)
                return false;
            if (top.compare_exchange_strong(curr_top, curr_top->next.load()))
            {
                item = curr_top->element;
                PushToRubbish(curr_top);
                return true;
            }
        }
    }
};

template <typename T>
using ConcurrentStack = LockFreeStack<T>;
